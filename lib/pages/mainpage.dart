import 'package:flutter/material.dart';

import 'package:myflutter/pages/chat.dart';
import 'package:myflutter/pages/friends.dart';
import 'package:myflutter/pages/zone.dart';

void main() => runApp(new MainPage());

class MainPage extends StatefulWidget {
  MainPage({Key key, }) : super(key: key);
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>{
  int _on = 0;
  List<BottomNavigationBarItem> _bottomNav;
  var appBarTitle = ['消息','联系人','动态'];

  @override
  void initState(){
    super.initState();
    
    _bottomNav = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.forum),
        title: Text('消息'),
      ),
      BottomNavigationBarItem(
        icon:Icon(Icons.person),
        title:Text('联系人')
      ),
      BottomNavigationBarItem(
        icon:Icon(Icons.grade),
        title:Text('动态')
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text(appBarTitle[_on]),
        centerTitle: true,
        leading: Padding(
          padding: EdgeInsets.all(6.5),
          child:ClipOval(
            child: Image.asset(
              'img/head.jpg',
              width:30,
              height: 30,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
      body: IndexedStack( //IndexedStack 组件，只会渲染选中(index)的页面
        index:_on,
        children: <Widget>[
          ChatPage(),
          FriendPage(),
          ZonePage()
        ],
      ),
      bottomNavigationBar:BottomNavigationBar(
        items: _bottomNav.map((BottomNavigationBarItem navigationView) => navigationView).toList(),
        currentIndex: _on,
        fixedColor: Colors.lightBlue,
        onTap: (index){
          setState(() {
           _on =index;
          });
        },
      )
    );
  }
}