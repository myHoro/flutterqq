import 'package:flutter/material.dart';
import 'dart:convert';
void main() => runApp(new FriendPage());

class FriendPage extends StatefulWidget {
  FriendPage({Key key, }) : super(key: key);
  @override
  _FriendPageState createState() => _FriendPageState();
}

List mylist = [
    {
      'group':'我的好友',
      'allNum':'56',
      'onlineNum':'30',
      'list':[
        {
          'name':'好友1',
          'headpic':'img/head3.jpg',
          'tips':'你好'
        },
        {
          'name':'好友2',
          'headpic':'img/head1.jpg',
          'tips':'他好'
        },
        {
          'name':'好友3',
          'headpic':'img/head2.jpg',
          'tips':'她好'
        },
      ]
    },
    {
      'group':'陌生人',
      'allNum':'12',
      'onlineNum':'3',
      'list':[
        {
          'name':'陌生人1',
          'headpic':'img/head3.jpg',
          'tips':'emmmm...'
        },
        {
          'name':'陌生人2',
          'headpic':'img/head1.jpg',
          'tips':'GKD'
        },
        {
          'name':'陌生人3',
          'headpic':'img/head2.jpg',
          'tips':'huh'
        },
      ]
    },
  ];

class _FriendPageState extends State<FriendPage>{
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: mylist.map((item){ //map((item){}); item返回的是元素，并非索引...
            return Column(
              children:[
                Container(
                  padding:EdgeInsets.fromLTRB(0, 8, 10, 8),
                  decoration: BoxDecoration(
                    border:Border(
                      bottom:BorderSide(width: 1, color: Colors.grey[200])
                    )
                  ),
                  child: Column(
                    children:[
                      Row(
                        children:[ 
                          Icon(
                            Icons.arrow_right,
                            color: Colors.grey,
                          ),
                          Expanded(
                            child: Text(item['group']),
                            flex: 1,
                          ),
                          Text(
                            item['onlineNum']+'/'+item['allNum'],
                            style: TextStyle(
                              color: Colors.grey
                            ),
                          )
                        ]
                      ),
                    ]
                  ) ,
                ),
              ]
            );
          }).toList() //toList(); children接受的是数组，所以要用toList()生成可用的List;
      ),
    );
  }

}