import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) { //build 构建UI界面的逻辑在build方法中
    return Scaffold(
      appBar:AppBar( // appBar：header标题
        title:Text('新路由'),
      ),
      body: Center( // 主体内容
        child: Text('这是新的页面'),
      ),
    );
  }
}
