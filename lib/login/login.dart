import 'package:flutter/material.dart';
import '../pages/chat.dart';


void main() => runApp(new Login());

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) { //build 构建UI界面的逻辑在build方法中

    Widget titleSection = Container( //这里都是没有状态变化的，直接创建在StatefulWidget里面
      child:Row(
        children: <Widget>[
          Container(
            height:135,
            padding:EdgeInsets.fromLTRB(15, 0, 5.0, 0),
            alignment: Alignment.bottomCenter,
            child:Image.asset(
              'img/qq.png',
              width:40,
              fit:BoxFit.cover,
            )
          ),
          Container(
            height:140,
            alignment: Alignment.bottomLeft,
            child:Text(
              'QQ',
              style: TextStyle(
                fontSize: 32
              ),
            )
          ),
        ],
      ),
    );

    Widget btmSection = Padding(
      padding:EdgeInsets.fromLTRB(16, 15, 16, 35),
      // alignment: Alignment.bottomCenter,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children:[
              Text(
                '忘记密码?',
                style: TextStyle(
                  color:Colors.lightBlue
                ),
              ),
              Text(
                '新用户注册',
                style: TextStyle(
                  color:Colors.lightBlue
                ),
              )
            ]
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                '登录即代表阅读并同意',
                style: TextStyle(
                  color: Colors.grey
                ),
              ),
              Text(
                '服务条款',
                style: TextStyle(
                  color:Colors.lightBlue
                ),
              ),
            ],
          ),
        ],
      ),
    );
  

    return new MaterialApp(
      title: 'Welcome to Flutter???',
      theme: new ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: new Scaffold(
        // appBar: new AppBar(
        //   title: new Text('登录'),
        // ),
        body:Flex(
          direction: Axis.vertical,
          children:[
            Expanded(
              flex:0,
              child:titleSection,
            ),
            Expanded(
              flex: 0,
              child:EnterInput(),
            ),
            Expanded(
              flex: 1,
              child: btmSection,
            ),
          ],
        )
      )
    );
  }
}

//class创建的是方法，在body里用 EnterInput() 方法调用渲染
class EnterInput extends StatefulWidget{ //账号密码框，根据不同状态要显示 清除及密码可见按钮，所以用StatefullWidget
  @override
  _EnterInputState createState() => new _EnterInputState();
}

class _EnterInputState extends State<EnterInput> {
  TextEditingController _account = TextEditingController(); //获取input框
  TextEditingController _password =TextEditingController();

  FocusNode _accountFocus = FocusNode(); //input获取焦点
  FocusNode _passFocus = FocusNode();

  bool _aActive =false, _pActive =false, _seeActive = true, _seeType =false;
  String _accountNum = '', _passwordNum = '';
  
  @override
  void initState(){
    _accountFocus.addListener((){ //监听帐号TextField 是否获得焦点
      if(_accountFocus.hasFocus){
        setState(() {
          _aActive = true;
        });
      }else{
        setState(() {
          _aActive =false;
        });
      }
    });
    _passFocus.addListener((){ //监听密码TextField 是否获得焦点
      if (_passFocus.hasFocus){
        setState(() {
          _pActive =true;
          _seeActive =false;
        });
      }else{
        setState(() {
          _pActive =false;
          _seeActive =true;
        });
      }
    });
    super.initState();
  }

  void _changeSeeType(){ //显示眼睛类型
    setState(() {
      _seeType = !_seeType;
    });
  }

  void _changeNum(e,txt){ //监听TextField 改变帐号密码
    setState(() {
      if(e == 'account'){
        _accountNum = e;
      }else{
        _passwordNum = e;
      }
    });
  }

  void _clearNum(String e){ //TextField 清除功能
    setState(() {
      if(e == 'account'){
        _accountNum = '';
        _account.clear();
      }else{
        _passwordNum = '';
        _password.clear();
      }
    });
  }
  
  void goLogin(){ //页面跳转
    if(_accountNum.length>=6&&_passwordNum.length>=6){
      Navigator.push(context, MaterialPageRoute(builder: (context) => MainPage() ));
    }else{
      
    }
  }

  @override
  Widget build(BuildContext context){
    return Container(
      padding:EdgeInsets.fromLTRB(15, 0, 15, 0),
      child:Column(
        children:[
          Container( //帐号
            alignment: Alignment.centerLeft,
            padding:EdgeInsets.fromLTRB(0, 30, 0, 0),
            decoration:BoxDecoration(
              border:Border(
                bottom: BorderSide(color:Color(0xFFd0d0d0),width:1)
              )
            ),
            child:Row(
              children:[
                Container(
                  height:45,
                  alignment: Alignment.centerLeft,
                  padding:EdgeInsets.fromLTRB(0, 0, 15, 0),
                  child:Text(
                    '帐号',
                    style:TextStyle(
                      fontSize: 20
                    ),
                  )
                ),
                new Expanded(
                  child:TextField(
                    controller: _account,
                    focusNode: _accountFocus,
                    keyboardType:TextInputType.number,
                    textInputAction: TextInputAction.next, // 右下角功能键
                    onChanged: (text) => {
                      _changeNum('account',text)
                    },
                    onEditingComplete:() => { //点击右下角触发事件
                      FocusScope.of(context).requestFocus(_passFocus), //密码框 onFocus
                    },
                    style:TextStyle(
                      fontSize: 20,
                    ),
                    decoration: InputDecoration(
                      hintText: 'QQ号/手机号/邮箱',
                      hintStyle: TextStyle(
                        fontSize: 20,
                        color:Color(0xFFd0d0d0),
                      ),
                      border: InputBorder.none,
                    ),
                  )
                ),
                Offstage(
                  offstage: _accountNum.length>0&&_aActive?false:true, //当值 为false显示，为true 隐藏
                  child: IconButton(
                    icon:Icon(Icons.clear),
                    color:Colors.grey,
                    onPressed:() => {
                      _clearNum('account')
                    },
                  ),
                ),
              ],
            )
          ),
          Container( //密码
            alignment: Alignment.centerLeft,
            padding:EdgeInsets.fromLTRB(0, 5, 0, 0),
            decoration:BoxDecoration(
              border:Border(
                bottom: BorderSide(color:Color(0xFFd0d0d0),width:1)
              )
            ),
            child:Row(
              children:[
                Container(
                  height:45,
                  alignment: Alignment.centerLeft,
                  padding:EdgeInsets.fromLTRB(0, 0, 15, 0),
                  child:Text(
                    '密码',
                    style:TextStyle(
                      fontSize: 20
                    ),
                  )
                ),
                new Expanded(
                  child:TextField(
                    controller: _password,
                    keyboardType:TextInputType.number,
                    obscureText:!_seeType, //是否隐藏输入值 == password
                    focusNode: _passFocus,
                    onChanged: (text) => {
                      _changeNum('password',text)
                    },
                    style:TextStyle(
                      fontSize: 20,
                    ),
                    decoration: InputDecoration(
                      hintText: '请填写密码',
                      hintStyle: TextStyle(
                        fontSize: 20,
                        color:Color(0xFFd0d0d0),
                      ),
                      border: InputBorder.none,
                    ),
                  )
                ),
                Offstage(
                  offstage: _passwordNum.length>0&&_pActive?false:true, //当值 为false显示，为true 隐藏
                  child: IconButton(
                    icon:Icon(Icons.clear),
                    color:Colors.grey,
                    onPressed:() => {
                      _clearNum('password')
                    },
                  ),
                ),
                Offstage(
                  offstage: _seeActive,
                  child: IconButton(
                    icon:_seeType?Icon(Icons.visibility):Icon(Icons.visibility_off),
                    color:Colors.grey,
                    onPressed: () => {
                      _changeSeeType()
                    },
                  ),
                )
              ],
            )
          ),
          Container(
            padding:EdgeInsets.fromLTRB(0, 20, 0, 0),
            child:Row(
              children:[
                Expanded(
                  child:RaisedButton(
                    onPressed: () => {
                      goLogin()
                    },
                    color:Colors.lightBlue,
                    textColor: Colors.white,
                    splashColor: Colors.lightBlueAccent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5))
                    ),
                    child:Padding(
                      padding:EdgeInsets.fromLTRB(0,12,0,12),
                      child: Text(
                        '登 录',
                        style: TextStyle(
                          fontSize: 18
                        ),
                      ),
                    )
                  )
                ),
              ],
            )
          ),
        ]
      )
    );
  }
}