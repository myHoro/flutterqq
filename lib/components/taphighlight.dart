import 'package:flutter/material.dart';

class TapHighLight extends StatefulWidget {
  TapHighLight({Key key, this.originalColor, @required this.child, @required this.onTap}):super(key:key);
  int originalColor = 0xFFffffff;
  final Widget child;
  final GestureTapCallback onTap;
  @override
  State<TapHighLight> createState() {
    return TapHighLightState();
  }
}

class TapHighLightState extends State<TapHighLight>{
  int active1 = 0xFFffffff;  
  bool on =false;

  @override
  Widget build(BuildContext context){
    return GestureDetector(
      onTapUp: (detail){
        if(on){
          setState(() {
            active1 = 0xFFffffff;
          });
        }
      },
      onTapDown: (detail){
        setState(() {
          active1 = 0xFFeeeeee;
          on =true;
        });
      },
      
      onTapCancel: ()=>{
        setState(() {
          active1 = 0xFFffffff;
        })
      },
      onTap: widget.onTap,
      child: Container(
        color: Color(active1),
        child: widget.child,
      ),
    );
  }
}